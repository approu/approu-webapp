import {get, writable} from 'svelte/store';

function persist(key, value) {
  localStorage.setItem(key, JSON.stringify(value));
}

export function writableLocalStorage(key, initialValue) {
  const LocalValue = JSON.parse(localStorage.getItem(key));
  if (!LocalValue) persist(key, initialValue);

  const store = writable(LocalValue || initialValue);
  store.subscribe(value => persist(key, value));
  return store;
}
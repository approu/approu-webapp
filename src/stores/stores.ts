import {writable, Writable} from "svelte/store";
import {writableLocalStorage} from "./localstorageutil";
import {writableSessionStorage} from "./sessionstorageutil";
import type {Gender} from "../dictionary/Gender";
import {Language} from "../dictionary/Language";
import type {Number} from "../dictionary/Number";
import type {Scope} from "../dictionary/Scope";
import type {LexicalCategory} from "../dictionary/LexicalCategory";
export const current_expanded_result_id = writable(-1);
export const is_online: Writable<boolean> = writable(true);
export const languages: Writable<Language[]> = writable([]);
export const current_dialect: Writable<Language> = writableLocalStorage(
    "current_dialect",
    new Language({
        "id": 2,
        "name": "Sanremasco",
        "short_name": "srm",
        "municipality_name": "Sanremo",
        "iso_code": "lij-x-srm",
        "disabled": false
    })
);
export const italian_language: Writable<Language> = writable(null);
export const genders: Writable<Gender[]> = writable([]);
export const numbers: Writable<Number[]> = writable([]);
export const lexical_categories: Writable<LexicalCategory[]> = writable([]);
export const scopes: Writable<Scope[]> = writable([]);

export const auth_token: Writable<string> = writableLocalStorage("token", "")


export function get_host() {
    const DEBUG = false;
    if (DEBUG) {
        return "http://localhost:8000";
    }
    else {
        return "";
    }
};

export const is_desktop_mediaquery = window.matchMedia("only screen and (max-width : 992px)");

export class Language {
    id: number;
    name: string;
    short_name: string;
    municipality_name: string;
    iso_code: string;
    disabled: boolean;

    constructor(json_obj) {
        this.id = json_obj["id"];
        this.name = json_obj["name"];
        this.short_name = json_obj["short_name"];
        this.municipality_name = json_obj["municipality_name"];
        this.iso_code = json_obj["iso_code"];
        this.disabled = json_obj["disabled"];
    }

    format_for_selectform():{value:number;text:string;}
    {
        return {
            value: this.id,
            text: this.name
        }
    }
}
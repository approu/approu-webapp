export class AudioPronunciation {
    id: number;
    audio: string;
    word: number;
    published: boolean;

    constructor(json_obj) {
        for (const key in json_obj) {
            this[key] = json_obj[key];
        }
    }
}

export class Word {
    id: number;
    word: string;
    definition: string;
    audio: string;
    neologism: boolean;
    language: number;
    gender: number;
    scope: number;
    lexical_category: number;
    constructor(json_obj) {
        for (const key in json_obj) {
            this[key] = json_obj[key];
        }
    }
}
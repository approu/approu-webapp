export class Number {
    id: number;
    number: string;

    constructor(json_obj) {
        for (const key in json_obj) {
            this[key] = json_obj[key];
        }
    }
}

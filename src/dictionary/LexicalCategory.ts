export class LexicalCategory {
    id: number;
    name: string;
    subtype: string;

    constructor(json_obj) {
        for (const key in json_obj) {
            this[key] = json_obj[key];
        }
    }
}

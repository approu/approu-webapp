export class Gender {
    id: number;
    gender: string;

    constructor(json_obj) {
        for (const key in json_obj) {
            this[key] = json_obj[key];
        }
    }
}

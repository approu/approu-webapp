import { is_online } from "../stores/stores";
import { get_host } from "../settings";

export function fetch_or_go_offline(path: string) : Promise<Response>{
   return fetch(
      get_host() + path)
   .catch((reason) => {
      is_online.set(false);
      return reason
   })
}

export function promise_or_go_offline(prom: Promise<any>) : Promise<Response>{
   return prom.catch((reason) => {
      is_online.set(false);
      return reason
   });
}
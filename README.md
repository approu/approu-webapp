# Appröu Webapp
This is a progressive web app PWA for the Appröu project.\
Check out the [official website of the project](https://www.approu.it)

## Building
Here are the build process steps
### Sass
1. Install sass compiler

	```
	sudo gem install sass
	```
1. Locate the directory using ```cd``` command
1. Compile and compress the resulting .css

	```
	sass --style compressed --scss sass/materialize.scss css/materialize.min.css
	```
1. Move resulting .css into ```public/materialize``` folder

### Svelte
Run ```npm install``` the first time

Then:

- to run a debug server, execute ```npm run dev```
- to build production files, execute ```npm run build```

## Copyright and license
©2021 Massimo Gismondi
The source code is licensed under the GPLv3 or later license.
Find the enclosed license in the LICENSE file.

This license does NOT grant you any right to use Appröu name, logo or other assets, nor acting in approu behalf.
